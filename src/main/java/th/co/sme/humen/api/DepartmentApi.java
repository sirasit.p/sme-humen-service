package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.anotation.CommonArgument;
import th.co.sme.humen.model.DepartmentModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.resolver.CommonParameter;
import th.co.sme.humen.service.DepartmentService;

import java.util.List;

@RestController
@RequestMapping("/department")
public class DepartmentApi {
    @Autowired
    DepartmentService departmentService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody DepartmentModel request) {
        return this.departmentService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody DepartmentModel request) {
        return this.departmentService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<DepartmentModel> findPage(@CommonArgument CommonParameter param) {
        return this.departmentService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<DepartmentModel> findAllList() {
        return this.departmentService.findAllList();
    }

    @GetMapping("/{departmentId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public DepartmentModel findById(@PathVariable(value = "departmentId") String departmentId) {
        return this.departmentService.findById(departmentId);
    }
}
