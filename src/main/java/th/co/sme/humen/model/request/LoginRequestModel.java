package th.co.sme.humen.model.request;

import lombok.Data;

@Data
public class LoginRequestModel {
    private String username;
    private String password;
}
