package th.co.sme.humen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import th.co.sme.humen.entity.MemberEntity;
import th.co.sme.humen.model.OrganizationModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.repository.MemberRepository;
import th.co.sme.humen.repository.OrganizationRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrganizationService {
    @Autowired
    OrganizationRepository organizationRepository;
    @Autowired
    MemberRepository memberRepository;

    @Transactional
    public StatusMessage create(OrganizationModel model) {
        this.organizationRepository.save(OrganizationModel.toEntity(model));
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage delete(OrganizationModel model) {
        this.organizationRepository.findById(model.getOrganizationId())
                .ifPresent(e -> {
//                    List<MemberEntity> list = this.memberRepository.findByOrganization(e)
//                            .peek(v -> v.setOrganization(null))
//                            .collect(Collectors.toList());
//                    this.memberRepository.saveAll(list);
                    this.organizationRepository.delete(e);
                });
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<OrganizationModel> findAllList() {
        return this.organizationRepository.findAll(Sort.by(Sort.Direction.ASC, "organizationId"))
                .stream()
                .map(OrganizationModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<OrganizationModel> findPage(PageRequest page) {
        return this.organizationRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "organizationId")))
                .map(OrganizationModel::fromEntity);
    }

    @Transactional
    public OrganizationModel findById(String organizationId) {
        return this.organizationRepository.findById(organizationId)
                .map(OrganizationModel::fromEntity)
                .orElseThrow(() -> new ResourceAccessException("Data " + organizationId + " not found"));
    }

}
