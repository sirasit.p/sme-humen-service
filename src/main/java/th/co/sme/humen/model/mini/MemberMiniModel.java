package th.co.sme.humen.model.mini;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.MemberEntity;
import th.co.sme.humen.model.*;

import java.util.Optional;
import java.util.UUID;

@Data
public class MemberMiniModel {
    private static Logger logger = LoggerFactory.getLogger(MemberMiniModel.class);
    private String memberId;
    private int prefix;
    private String thFirstname;
    private String engFirstname;
    private String thLastname;
    private String engLastname;
    private String picture;
    private OrganizationModel organization;

    public String getThFullName() {
        return this.getThFirstname() + " " + this.getThLastname();
    }

    public String getEngFullName() {
        return this.getEngFirstname() + " " + this.getEngLastname();
    }

    public static MemberMiniModel fromEntity(MemberEntity entity) {
        if (entity == null) {
            return null;
        }
        MemberMiniModel model = new MemberMiniModel();
        try {
            model.setMemberId(entity.getMemberId());
            model.setPrefix(entity.getPrefix());
            model.setThFirstname(entity.getThFirstname());
            model.setEngFirstname(entity.getEngFirstname());
            model.setThLastname(entity.getThLastname());
            model.setEngLastname(entity.getEngLastname());
            model.setPicture(entity.getPicture());
            if (Optional.ofNullable(entity.getOrganization()).isPresent()) {
                model.setOrganization(OrganizationModel.fromEntity(entity.getOrganization()));

            } else {
                model.setOrganization(null);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static MemberEntity toEntity(MemberMiniModel model) {
        if (model == null) {
            return null;
        }
        MemberEntity entity = new MemberEntity();
        try {
            entity.setMemberId(model.getMemberId());
            entity.setPrefix(model.getPrefix());
            entity.setThFirstname(model.getThFirstname());
            entity.setEngFirstname(model.getEngFirstname());
            entity.setThLastname(model.getThLastname());
            entity.setEngLastname(model.getEngLastname());
            entity.setPicture(model.getPicture());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
