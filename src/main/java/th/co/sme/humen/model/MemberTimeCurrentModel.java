package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.model.mini.MemberMiniModel;

import java.util.List;

@Data
public class MemberTimeCurrentModel {
    private static Logger logger = LoggerFactory.getLogger(MemberTimeCurrentModel.class);
    private MemberMiniModel member;
    private List<MemberEventgrpModel> eventgrp;
    private List<TimeCurrentModel> time;


    public static MemberTimeCurrentModel fromModel(MemberMiniModel member, List<TimeCurrentModel> timeCurrentList, List<MemberEventgrpModel> eventgrp) {
        MemberTimeCurrentModel model = new MemberTimeCurrentModel();
        try {
            model.setMember(member);
            model.setEventgrp(eventgrp);
            model.setTime(timeCurrentList);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return model;
    }
}
