package th.co.sme.humen.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import th.co.myhr.system.security.service.TokenService;
import th.co.myhr.system.security.service.impl.JWTTokenService;

@Configuration
public class TokenConfig {

    @Value("${sme.access.token.secret}")
    private String accessSecret;
    @Value("${sme.access.token.expiration:1}")
    private long accessExpiration;
    @Value("${sme.refresh.token.secret}")
    private String refreshSecret;
    @Value("${sme.refresh.token.expiration:0}")
    private long refreshExpiration;

    @Bean
    public TokenService getAccessTokenService() {
        return new JWTTokenService(accessSecret, accessExpiration);
    }

    @Bean
    public TokenService getRefreshTokenService() {
        return new JWTTokenService(refreshSecret, refreshExpiration);
    }
}
