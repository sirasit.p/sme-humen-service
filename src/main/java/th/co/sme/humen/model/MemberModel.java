package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.MemberEntity;

import java.util.Optional;
import java.util.UUID;

@Data
public class MemberModel {
    private static Logger logger = LoggerFactory.getLogger(MemberModel.class);
    private String memberId;
    private int prefix;
    private String thFirstname;
    private String engFirstname;
    private String thLastname;
    private String engLastname;
    private int gender;
    private String email;
    private String tel;
    private String picture;
    private String code;
    private String area;
    private int status;
    private DepartmentModel department;
    private PositionModel position;
    private JobModel job;
    private EventgrpModel eventGrp;
    private OrganizationModel organizationModel;

    public String getThFullName() {
        return this.getThFirstname() + " " + this.getThLastname();
    }

    public String getEngFullName() {
        return this.getEngFirstname() + " " + this.getEngLastname();
    }

    public static MemberModel fromEntity(MemberEntity entity) {
        if (entity == null) {
            return null;
        }
        MemberModel model = new MemberModel();
        try {
            model.setMemberId(entity.getMemberId());
            model.setPrefix(entity.getPrefix());
            model.setThFirstname(entity.getThFirstname());
            model.setEngFirstname(entity.getEngFirstname());
            model.setThLastname(entity.getThLastname());
            model.setEngLastname(entity.getEngLastname());
            model.setGender(entity.getGender());
            model.setEmail(entity.getEmail());
            model.setTel(entity.getTel());
            model.setPicture(entity.getPicture());
            model.setCode(entity.getCode());
            model.setArea(entity.getArea());
            model.setStatus(entity.getStatus());
            model.setDepartment(DepartmentModel.fromEntity(entity.getDepartment()));
            model.setPosition(PositionModel.fromEntity(entity.getPosition()));
            model.setJob(JobModel.fromEntity(entity.getJob()));
            model.setEventGrp(EventgrpModel.fromEntity(entity.getEventgrp()));
            model.setOrganizationModel(OrganizationModel.fromEntity(entity.getOrganization()));

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static MemberEntity toEntity(MemberModel model) {
        if (model == null) {
            return null;
        }
        MemberEntity entity = new MemberEntity();
        try {
            if (Optional.ofNullable(model.getMemberId()).isEmpty() || model.getMemberId().isEmpty()) {
                model.setMemberId(UUID.randomUUID().toString());
            }
            entity.setMemberId(model.getMemberId());
            entity.setPrefix(model.getPrefix());
            entity.setThFirstname(model.getThFirstname());
            entity.setEngFirstname(model.getEngFirstname());
            entity.setThLastname(model.getThLastname());
            entity.setEngLastname(model.getEngLastname());
            entity.setGender(model.getGender());
            entity.setEmail(model.getEmail());
            entity.setTel(model.getTel());
            entity.setPicture(model.getPicture());
            entity.setCode(model.getCode());
            entity.setArea(model.getArea());
            entity.setStatus(model.getStatus());
            entity.setDepartment(DepartmentModel.toEntity(model.getDepartment()));
            entity.setPosition(PositionModel.toEntity(model.getPosition()));
            entity.setJob(JobModel.toEntity(model.getJob()));
            entity.setEventgrp(EventgrpModel.toEntity(model.getEventGrp()));
            entity.setOrganization(OrganizationModel.toEntity(model.getOrganizationModel()));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
