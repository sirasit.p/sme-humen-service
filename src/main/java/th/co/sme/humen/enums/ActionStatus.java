package th.co.sme.humen.enums;

public enum ActionStatus {
    FAIL(-1),
    NONE(0),
    SUCCESS(1);

    private int state = 0;

    private ActionStatus(int state) {
        this.state = state;
    }

    public int getState() {
        return this.state;
    }

    public boolean isSuccess() {
        return this.state == ActionStatus.SUCCESS.getState();
    }

    public boolean isFail() {
        return this.state == ActionStatus.FAIL.getState();
    }

    public boolean isNone() {
        return this.state == ActionStatus.NONE.getState();
    }
}
