package th.co.sme.humen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import th.co.sme.humen.enums.EnableStatus;
import th.co.sme.humen.exception.ResourceNotFoundException;
import th.co.myhr.system.security.model.UProfile;
import th.co.sme.humen.entity.UsersEntity;
import th.co.sme.humen.model.*;
import th.co.sme.humen.repository.MemberRepository;
import th.co.sme.humen.repository.OrganizationRepository;
import th.co.sme.humen.repository.UsersRepository;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MemberService {
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    SendMailService sendMailService;
    @Autowired
    OrganizationRepository organizationRepository;

    @Transactional
    public StatusMessage create(MemberModel model) {
        this.memberRepository.save(MemberModel.toEntity(model));
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage delete(MemberModel model) {
        this.memberRepository.findById(model.getMemberId())
                .ifPresent(e -> {
                    this.memberRepository.delete(e);
                });
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<MemberModel> findAllList() {
        return this.memberRepository.findAll(Sort.by(Sort.Direction.ASC, "thFirstname", "thLastname", "engFirstname", "engLastname"))
                .stream()
                .map(e -> {
                    if (e.getOrganization() == null || e.getOrganization().getOrganizationId().isEmpty()) {
                        this.organizationRepository.findById("100").ifPresent(organization -> {
                            e.setOrganization(organization);
                            this.memberRepository.save(e);
                        });

                    }
                    return e;
                })
                .map(MemberModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<MemberModel> findPage(PageRequest page) {
        return this.memberRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "thFirstname", "thLastname", "engFirstname", "engLastname")))
                .map(MemberModel::fromEntity);
    }

    @Transactional
    public MemberModel findById(String memberId) {
        return this.memberRepository.findById(memberId)
                .map(MemberModel::fromEntity)
                .orElseThrow(() -> new ResourceNotFoundException("Data " + memberId + " not found"));
    }

    @Transactional
    public ProfileModel findByProfile(UProfile up) {
        UsersEntity users = this.usersRepository.findById(up.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("Data " + up.getUsername() + " not found"));
        ProfileModel profile = ProfileModel.fromEntity(users.getMember());
        profile.setRole(UsersRoleModel.fromEntity(users.getRole()));
        return profile;
    }

    @Transactional
    public List<MemberModel> waitingCheck(int status) {
        return this.memberRepository.findByStatus(status, Sort.by(Sort.Direction.ASC, "createdDate"))
                .map(MemberModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public long countByStatus() {
        return this.memberRepository.countByStatus(EnableStatus.DISABLE.getValue());
    }

    @Transactional
    public StatusMessage approveMember(MemberModel member) throws MessagingException {
        if (member.getStatus() == EnableStatus.ENABLE.getValue()) {
            this.memberRepository.findById(member.getMemberId())
                    .ifPresent(e -> {
                        e.setStatus(EnableStatus.ENABLE.getValue());
                        this.memberRepository.save(e);
                    });
            String subject = " SME-HUMEN ";
            String content = "Your profile is approved.<br>";
            this.sendMailService.sendEmail(new String[]{member.getEmail()}, new String[]{}, subject, content);

        } else if (member.getStatus() == EnableStatus.DISABLE.getValue()) {
            this.memberRepository.findById(member.getMemberId())
                    .ifPresent(e -> {
                        e.setStatus(EnableStatus.DISABLE.getValue());
                        this.memberRepository.save(e);
                    });
            String subject = " SME-HUMEN ";
            String content = " Your profile has been disabled. ";
            this.sendMailService.sendEmail(new String[]{member.getEmail()}, new String[]{}, subject, content);
        }
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }
}
