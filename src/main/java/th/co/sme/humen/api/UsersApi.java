package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.anotation.CommonArgument;
import th.co.sme.humen.model.UsersModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.model.request.ChangePasswordRequestModel;
import th.co.sme.humen.resolver.CommonParameter;
import th.co.sme.humen.service.UsersService;
import th.co.myhr.security.anotation.UProfileArgument;
import th.co.myhr.system.security.model.UProfile;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersApi {
    @Autowired
    UsersService usersService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody UsersModel request) {
        return this.usersService.create(request);
    }

    @PostMapping("/change-password")
    @Parameter(name = "up", hidden = true)
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage changePassword(@RequestBody ChangePasswordRequestModel request
            , @UProfileArgument UProfile up) {
        return this.usersService.changePassword(up.getUsername(), request.getOldPassword(), request.getPassword());
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody UsersModel request) {
        return this.usersService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<UsersModel> findPage(@CommonArgument CommonParameter param) {
        return this.usersService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<UsersModel> findAllList() {
        return this.usersService.findAllList();
    }

    @GetMapping("/{username}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public UsersModel findById(@PathVariable(value = "username") String username) {
        return this.usersService.findById(username);
    }
}
