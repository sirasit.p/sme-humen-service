package th.co.sme.humen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.sme.humen.entity.Eventgrp1Entity;
import th.co.sme.humen.entity.EventgrpEntity;
import th.co.sme.humen.entity.identity.Eventgrp1Identity;

import java.util.stream.Stream;

@Repository
public interface Eventgrp1Repository extends JpaRepository<Eventgrp1Entity, Eventgrp1Identity> {
    Stream<Eventgrp1Entity> findByIdentityEventgrp(EventgrpEntity entity);

    Stream<Eventgrp1Entity> findByIdentityEventgrpEventgrpId(String eventgrpId);
}
