FROM openjdk:17-alpine
RUN apk add --no-cache tzdata
VOLUME /tmp
#ARG JAR_FILE
#COPY ${JAR_FILE} app.jar
ADD target/sme-humen-service-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]