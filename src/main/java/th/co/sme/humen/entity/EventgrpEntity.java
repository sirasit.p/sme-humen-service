package th.co.sme.humen.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Entity
@Table(name = "eventgrp")
public class EventgrpEntity extends AbstractAuditingEntity {

    @Id
    @NotNull
    @Column(name = "eventgrp_id")
    private String eventgrpId;
    @Column(name = "th_name", columnDefinition = "TEXT")
    private String thName;
    @Column(name = "eng_name", columnDefinition = "TEXT")
    private String engName;

    @OneToMany(mappedBy = "identity.eventgrp", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Eventgrp1Entity> eventgrp1List;

    public EventgrpEntity(String eventgrpId) {
        this.eventgrpId = eventgrpId;
    }

    public EventgrpEntity() {
    }
}
