package th.co.sme.humen.model.request;

import lombok.Data;

@Data
public class TimeCurrentRequestModel {
    private String memberId;
    private String departmentId;
    private String positionId;
    private String jobId;
    private String startDate;
    private String endDate;
}
