package th.co.sme.humen.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.sme.humen.entity.TagEntity;

import java.util.stream.Stream;

@Repository
public interface TagRepository extends JpaRepository<TagEntity, String> {
    Stream<TagEntity> findByValueContaining(String textSearch, Sort sort);
}
