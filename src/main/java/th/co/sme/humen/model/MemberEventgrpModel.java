package th.co.sme.humen.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
@EqualsAndHashCode(callSuper = true)
public class MemberEventgrpModel extends EventgrpDetailModel {
    private static Logger logger = LoggerFactory.getLogger(MemberEventgrpModel.class);
    private int used;
    private int remain;

    public static MemberEventgrpModel fromEntity(EventgrpDetailModel eventgrpDetail, int useEventgrp) {
        if (eventgrpDetail == null) {
            return null;
        }
        MemberEventgrpModel model = new MemberEventgrpModel();
        try {
            model.setType(eventgrpDetail.getType());
            model.setThName(eventgrpDetail.getThName());
            model.setEngName(eventgrpDetail.getEngName());
            model.setLimits(eventgrpDetail.getLimits());
            model.setUsed(useEventgrp);
            model.setRemain(eventgrpDetail.getLimits() - useEventgrp);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

}
