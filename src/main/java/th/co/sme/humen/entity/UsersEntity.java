package th.co.sme.humen.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Collection;

@Data
@Entity
@Table(name = "users")
public class UsersEntity extends AbstractAuditingEntity implements UserDetails {
    @Id
    @NotNull
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "temp_password")
    private String tempPassword;
    @Column(name = "temp_password_expire")
    private Instant tempPasswordExpire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private MemberEntity member;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")
    private UsersRoleEntity role;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
