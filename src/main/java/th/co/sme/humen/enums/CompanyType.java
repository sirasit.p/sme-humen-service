package th.co.sme.humen.enums;

public enum CompanyType {
    DEFAULT(0), //ไม่ได้กำหนด
    PRODUCTION_BUSINESS(9), //กิจการด้านการผลิต 1
    WHOLESALE_AND_RETAIL_BUSINESS(10), //กิจการด้านค้าส่งค้าปลีก 2
    SERVICE_BUSINESS(11); //กิจการด้านบริการ 3

    private final int id;

    CompanyType(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }

    public static CompanyType as(String str) {
        for (CompanyType me : CompanyType.values()) {
            if (me.name().equalsIgnoreCase(str))
                return me;
        }
        return PRODUCTION_BUSINESS;
    }
}
