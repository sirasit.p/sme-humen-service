package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.anotation.CommonArgument;
import th.co.sme.humen.model.JobModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.resolver.CommonParameter;
import th.co.sme.humen.service.JobService;

import java.util.List;

@RestController
@RequestMapping("/job")
public class JobApi {
    @Autowired
    JobService jobService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody JobModel request) {
        return this.jobService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody JobModel request) {
        return this.jobService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<JobModel> findPage(@CommonArgument CommonParameter param) {
        return this.jobService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<JobModel> findAllList() {
        return this.jobService.findAllList();
    }

    @GetMapping("/{jobId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public JobModel findById(@PathVariable(value = "jobId") String jobId) {
        return this.jobService.findById(jobId);
    }
}
