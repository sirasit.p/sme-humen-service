package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.anotation.CommonArgument;
import th.co.sme.humen.model.OrganizationModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.resolver.CommonParameter;
import th.co.sme.humen.service.OrganizationService;

import java.util.List;

@RestController
@RequestMapping("/organization")
public class OrganizationApi {
    @Autowired
    OrganizationService organizationService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody OrganizationModel request) {
        return this.organizationService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody OrganizationModel request) {
        return this.organizationService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<OrganizationModel> findPage(@CommonArgument CommonParameter param) {
        return this.organizationService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<OrganizationModel> findAllList() {
        return this.organizationService.findAllList();
    }

    @GetMapping("/{organizationId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public OrganizationModel findById(@PathVariable(value = "organizationId") String organizationId) {
        return this.organizationService.findById(organizationId);
    }
}
