package th.co.sme.humen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import th.co.sme.humen.entity.Eventgrp1Entity;
import th.co.sme.humen.entity.EventgrpEntity;
import th.co.sme.humen.entity.identity.Eventgrp1Identity;
import th.co.sme.humen.exception.ResourceNotFoundException;
import th.co.sme.humen.model.EventgrpDetailModel;
import th.co.sme.humen.model.EventgrpModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.repository.Eventgrp1Repository;
import th.co.sme.humen.repository.EventgrpRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventgrpService {
    @Autowired
    EventgrpRepository eventgrpRepository;
    @Autowired
    Eventgrp1Repository eventgrp1Repository;

    @Transactional
    public StatusMessage create(EventgrpModel model) {
        EventgrpEntity entity = this.eventgrpRepository.save(EventgrpModel.toEntity(model));
        List<Eventgrp1Entity> newEventgrpDetail = EventgrpDetailModel.toEntity(model.getDetail());
        for (Eventgrp1Entity eventgrp1 : newEventgrpDetail) {
            eventgrp1.setIdentity(new Eventgrp1Identity(entity, eventgrp1.getIdentity().getType()));
        }

        this.eventgrp1Repository.saveAll(newEventgrpDetail);

        List<EventgrpDetailModel> newChangeModel = model.getDetail();
        if (newChangeModel == null) {
            newChangeModel = new ArrayList<>();
        }

        List<EventgrpDetailModel> oldChangeModel = this.eventgrp1Repository.findByIdentityEventgrp(entity)
                .map(EventgrpDetailModel::fromEntity)
                .collect(Collectors.toList());

        for (EventgrpDetailModel old : oldChangeModel) {
            boolean isDelete = true;
            for (EventgrpDetailModel news : newChangeModel) {
                boolean isType = old.getType().equals(news.getType());
                if (isType) {
                    isDelete = false;
                    break;
                }
            }

            if (isDelete) {
                this.eventgrp1Repository.findById(new Eventgrp1Identity(entity, old.getType()))
                        .ifPresent(m -> {
                            this.eventgrp1Repository.delete(m);
                        });
            }
        }

        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage delete(EventgrpModel model) {
        this.eventgrpRepository.findById(model.getEventgrpId())
                .ifPresent(e -> {
                    this.eventgrpRepository.delete(e);
                });
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public EventgrpModel findById(String eventgrpId) {
        return this.eventgrpRepository.findById(eventgrpId)
                .map(EventgrpModel::fromEntity)
                .orElseThrow(() -> new ResourceNotFoundException("Data eventgrpId : " + eventgrpId + " not found"));
    }

    @Transactional
    public List<EventgrpModel> findAllList() {
        return this.eventgrpRepository.findAll(Sort.by(Sort.Direction.ASC, "eventgrpId"))
                .stream()
                .map(EventgrpModel::fromEntity).collect(Collectors.toList());
    }


    @Transactional
    public List<EventgrpDetailModel> findByIdentityEventgrpEventgrpId(String eventgrpId) {
        return this.eventgrp1Repository.findByIdentityEventgrpEventgrpId(eventgrpId)
                .map(EventgrpDetailModel::fromEntity).collect(Collectors.toList());
    }
}
