package th.co.sme.humen.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import th.co.sme.humen.entity.DepartmentEntity;
import th.co.sme.humen.entity.MemberEntity;
import th.co.sme.humen.entity.TimeCurrentEntity;
import th.co.sme.humen.entity.identity.TimeCurrentIdentity;

import java.util.List;
import java.util.stream.Stream;

@Repository
public interface TimeCurrentRepository extends JpaRepository<TimeCurrentEntity, TimeCurrentIdentity> {
    @Query(value = "select distinct(tc.member_id) from time_current tc " +
            "where member_id like '%' " +
            "  and ((1 = :param1 and member_id in (select member_id from member where organization_id = :organizationId)) or (1 = :param2)) " +
            "  and ((1 = :param3 and member_id in (select member_id from member where department_id = :departmentId)) or (1 = :param4)) " +
            "  and ((1 = :param5 and member_id in (select member_id from member where position_id = :positionId)) or (1 = :param6)) " +
            "  and ((1 = :param7 and member_id in (select member_id from member where job_id = :jobId)) or (1 = :param8)) " +
            "  and date_id between :startDate and :endDate ", nativeQuery = true)
    List<String> findByTimeFilter(@Param("organizationId") String organizationId
            , @Param("departmentId") String departmentId
            , @Param("positionId") String positionId
            , @Param("jobId") String jobId
            , @Param("startDate") String startDate
            , @Param("endDate") String endDate
            , @Param("param1") int param1
            , @Param("param2") int param2
            , @Param("param3") int param3
            , @Param("param4") int param4
            , @Param("param5") int param5
            , @Param("param6") int param6
            , @Param("param7") int param7
            , @Param("param8") int param8);

    Stream<TimeCurrentEntity> findByIdentityMemberAndIdentityDateIdBetween(MemberEntity member, String startDate, String endDate, Sort sort);

    Stream<TimeCurrentEntity> findByIdentityMember(MemberEntity member);

    long countByIdentityMemberMemberIdAndIdentityDateIdBetweenAndEventgrpType(String memberId, String startDate, String endDate, String eventgrpType);
}
