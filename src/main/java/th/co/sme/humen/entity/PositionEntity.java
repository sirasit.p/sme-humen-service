package th.co.sme.humen.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "position")
public class PositionEntity extends AbstractAuditingEntity {
    @Id
    @NotNull
    @Column(name = "position_id")
    private String positionId;
    @Column(name = "th_name", columnDefinition = "TEXT")
    private String thName;
    @Column(name = "eng_name", columnDefinition = "TEXT")
    private String engName;
}
