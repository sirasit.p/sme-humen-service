package th.co.sme.humen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.sme.humen.entity.JobEntity;

@Repository
public interface JobRepository extends JpaRepository<JobEntity, String> {
}
