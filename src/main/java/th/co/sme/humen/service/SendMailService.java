package th.co.sme.humen.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class SendMailService {
    private static Logger logger = LoggerFactory.getLogger(SendMailService.class);

    @Value("${spring.mail.username}")
    private String mailForm;

    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    FilesStorageService filesStorageService;

    @Async
    public void sendEmail(String[] to, String[] cc, String subject, String content) {
        try {
            MimeMessage msg = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(msg, true);
            helper.setFrom(mailForm);
            helper.setTo(to);
            helper.setCc(cc);
            helper.setSubject(subject);
            helper.setText(content, true);
            javaMailSender.send(msg);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    @Async
    public void sendEmailWithAttachment(String[] to, String[] cc, String subject, String content, String path, String fileName) {
        try {
            MimeMessage msg = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(msg, true);
            helper.setFrom(mailForm);
            helper.setTo(to);
            helper.setCc(cc);
            helper.setSubject(subject);
            helper.setText(content, true);
            this.filesStorageService.init(path);
            Resource file = this.filesStorageService.load(fileName);
            helper.addAttachment(fileName, file.getFile());
            javaMailSender.send(msg);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
}
