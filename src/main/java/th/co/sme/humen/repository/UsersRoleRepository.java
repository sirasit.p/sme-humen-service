package th.co.sme.humen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.sme.humen.entity.UsersRoleEntity;

@Repository
public interface UsersRoleRepository extends JpaRepository<UsersRoleEntity, String> {
}
