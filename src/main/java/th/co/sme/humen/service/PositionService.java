package th.co.sme.humen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import th.co.sme.humen.entity.MemberEntity;
import th.co.sme.humen.exception.ResourceNotFoundException;
import th.co.sme.humen.model.PositionModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.repository.MemberRepository;
import th.co.sme.humen.repository.PositionRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PositionService {
    @Autowired
    PositionRepository positionRepository;
    @Autowired
    MemberRepository memberRepository;

    @Transactional
    public StatusMessage create(PositionModel model) {
        if (model.getPositionId() != null && !model.getPositionId().isEmpty()) {
            this.positionRepository.save(PositionModel.toEntity(model));
            return StatusMessage.createInstanceSuccess("Data saved successfully.");
        } else {
            return StatusMessage.createInstanceFail("Data saved unsuccessfully.");
        }
    }

    @Transactional
    public StatusMessage delete(PositionModel model) {
        this.positionRepository.findById(model.getPositionId())
                .ifPresent(e -> {
                    List<MemberEntity> list = this.memberRepository.findByPosition(e)
                            .peek(v -> v.setPosition(null))
                            .collect(Collectors.toList());
                    this.memberRepository.saveAll(list);
                    this.positionRepository.delete(e);
                });
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<PositionModel> findAllList() {
        return this.positionRepository.findAll(Sort.by(Sort.Direction.ASC, "positionId"))
                .stream()
                .map(PositionModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<PositionModel> findPage(PageRequest page) {
        return this.positionRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "positionId")))
                .map(PositionModel::fromEntity);
    }

    @Transactional
    public PositionModel findById(String positionId) {
        return this.positionRepository.findById(positionId)
                .map(PositionModel::fromEntity)
                .orElseThrow(() -> new ResourceNotFoundException("Data " + positionId + " not found"));
    }
}
