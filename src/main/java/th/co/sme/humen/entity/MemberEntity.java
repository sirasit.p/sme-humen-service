package th.co.sme.humen.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Entity
@Table(name = "member")
public class MemberEntity extends AbstractAuditingEntity {
    @Id
    @NotNull
    @Column(name = "member_id")
    private String memberId;
    @Column(name = "prefix")
    private int prefix;
    @Column(name = "th_firstname", columnDefinition = "TEXT")
    private String thFirstname;
    @Column(name = "eng_firstname", columnDefinition = "TEXT")
    private String engFirstname;
    @Column(name = "th_lastname", columnDefinition = "TEXT")
    private String thLastname;
    @Column(name = "eng_lastname", columnDefinition = "TEXT")
    private String engLastname;
    @Column(name = "gender")
    private int gender;
    @Column(name = "email")
    private String email;
    @Column(name = "tel")
    private String tel;
    @Column(name = "picture")
    private String picture;
    @Column(name = "code")
    private String code;
    @Column(name = "area")
    private String area;
    @Column(name = "status")
    private int status;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private DepartmentEntity department;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "position_id")
    private PositionEntity position;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "job_id")
    private JobEntity job;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eventgrp_id")
    private EventgrpEntity eventgrp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id")
    private OrganizationEntity organization;

    @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UsersEntity> users;
}
