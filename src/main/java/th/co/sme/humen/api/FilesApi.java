package th.co.sme.humen.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.model.request.FilesUploadRequestModel;
import th.co.sme.humen.service.FilesService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/files")
public class FilesApi {
    @Autowired
    FilesService filesService;

    @PostMapping("/upload-image")
    public StatusMessage uploadImage(@RequestParam("file") MultipartFile file) {
        return this.filesService.uploadImage(file);
    }

    @GetMapping(value = "/image/{filename}")
    public ResponseEntity openImage(@PathVariable(value = "filename") String filename) throws IOException {
        return this.getResponseFromData(this.filesService.openImage(filename));
    }

//    @PostMapping("/upload-video")
//    public StatusMessage uploadVideo(@RequestParam("file") MultipartFile file) {
//        return this.filesService.uploadVideo(file);
//    }
//
//    @GetMapping(value = "/video/{filename}", produces = "video/mp4")
//    public FileSystemResource openVideo(@PathVariable(value = "filename") String filename) throws IOException {
//        return this.filesService.openVideo(filename);
//    }
//
//    @PostMapping("/upload-pdf")
//    public StatusMessage uploadPdf(@RequestParam("file") MultipartFile file) {
//        return this.filesService.uploadPdf(file);
//    }
//
//    @GetMapping(value = "/pdf/{filename}", produces = "application/pdf")
//    public FileSystemResource openPdf(@PathVariable(value = "filename") String filename) throws IOException {
//        return this.filesService.openPdf(filename);
//    }
//
//    @PostMapping("/to-base64")
//    public StatusMessage convertFileToBase64(@RequestParam("file") MultipartFile file) throws IOException {
//        return this.filesService.convertFileToBase64(file);
//    }
//
//    @PostMapping("/upload-base64")
//    public StatusMessage uploadBase64(@RequestBody FilesUploadRequestModel request) {
//        return this.filesService.uploadBase64(request);
//    }
//
//    @PostMapping("/upload-certificate/{filename}")
//    public StatusMessage uploadCertificate(@RequestParam("file") MultipartFile file, @PathVariable(value = "filename") String filename) {
//        return this.filesService.uploadCertificate(file, filename);
//    }
//
//    @PostMapping("/upload-certificate-base64")
//    public StatusMessage uploadCertificateBase64(@RequestBody FilesUploadRequestModel request) {
//        return this.filesService.uploadCertificateBase64(request);
//    }
//
//    @GetMapping(value = "/certificate/{filename}", produces = MediaType.APPLICATION_PDF_VALUE)
//    public FileSystemResource openCertificate(@PathVariable(value = "filename") String filename) throws IOException {
//        return this.filesService.openCertificate(filename);
//    }

    @PostMapping("/upload")
    public StatusMessage upload(@RequestParam("file") MultipartFile file) {
        return this.filesService.upload(file);
    }

    @GetMapping(value = "/download/{filename}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public FileSystemResource download(@PathVariable(value = "filename") String filename, HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment; filename=" + filename);
        return this.filesService.download(filename);
    }

    private ResponseEntity<byte[]> getResponseFromData(byte[] data) {
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.maxAge(300, TimeUnit.MINUTES));
        headers.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<>(data, headers, HttpStatus.OK);
    }
}
