package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.model.EventgrpDetailModel;
import th.co.sme.humen.model.EventgrpModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.service.EventgrpService;

import java.util.List;

@RestController
@RequestMapping("/eventgrp")
public class EventgrpApi {
    @Autowired
    EventgrpService eventgrpService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody EventgrpModel request) {
        return this.eventgrpService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody EventgrpModel request) {
        return this.eventgrpService.delete(request);
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<EventgrpModel> findAllList() {
        return this.eventgrpService.findAllList();
    }

    @GetMapping("/full/{eventgrpId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public EventgrpModel findById(@PathVariable(value = "eventgrpId") String eventgrpId) {
        return this.eventgrpService.findById(eventgrpId);
    }

    @GetMapping("/detail/{eventgrpId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<EventgrpDetailModel> findByIdentityEventgrpEventgrpId(@PathVariable(value = "eventgrpId") String eventgrpId) {
        return this.eventgrpService.findByIdentityEventgrpEventgrpId(eventgrpId);
    }
}
