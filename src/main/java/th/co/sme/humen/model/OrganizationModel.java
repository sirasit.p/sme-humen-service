package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.OrganizationEntity;

import javax.persistence.Column;
import java.util.List;

@Data
public class OrganizationModel {
    private static Logger logger = LoggerFactory.getLogger(OrganizationModel.class);
    private String organizationId;
    private String thName;
    private String engName;
    private String picture;
    private String details;
    private double startTime;
    private double endTime;

    public static OrganizationModel fromEntity(OrganizationEntity entity) {
        if (entity == null) {
            return null;
        }
        OrganizationModel model = new OrganizationModel();
        try {
            model.setOrganizationId(entity.getOrganizationId());
            model.setThName(entity.getThName());
            model.setEngName(entity.getEngName());
            model.setPicture(entity.getPicture());
            model.setDetails(entity.getDetails());
            model.setStartTime(entity.getStartTime());
            model.setEndTime(entity.getEndTime());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static OrganizationEntity toEntity(OrganizationModel model) {
        if (model == null) {
            return null;
        }
        OrganizationEntity entity = new OrganizationEntity();
        try {
            entity.setOrganizationId(model.getOrganizationId());
            entity.setThName(model.getThName());
            entity.setEngName(model.getEngName());
            entity.setPicture(model.getPicture());
            entity.setDetails(model.getDetails());
            entity.setStartTime(model.getStartTime());
            entity.setEndTime(model.getEndTime());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
