package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.PositionEntity;

@Data
public class PositionModel {
    private static Logger logger = LoggerFactory.getLogger(PositionModel.class);
    private String positionId;
    private String thName;
    private String engName;

    public static PositionModel fromEntity(PositionEntity entity) {
        if (entity == null) {
            return null;
        }
        PositionModel model = new PositionModel();
        try {
            model.setPositionId(entity.getPositionId());
            model.setThName(entity.getThName());
            model.setEngName(entity.getEngName());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static PositionEntity toEntity(PositionModel model) {
        if (model == null) {
            return null;
        }
        PositionEntity entity = new PositionEntity();
        try {
            entity.setPositionId(model.getPositionId());
            entity.setThName(model.getThName());
            entity.setEngName(model.getEngName());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
