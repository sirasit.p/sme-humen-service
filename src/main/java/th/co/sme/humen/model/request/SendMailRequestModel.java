package th.co.sme.humen.model.request;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public class SendMailRequestModel {
    private static Logger logger = LoggerFactory.getLogger(SendMailRequestModel.class);
    private String[] to = new String[]{};
    private String[] cc = new String[]{};
    private String subject;
    private String content;
}
