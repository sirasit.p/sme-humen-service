package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.DepartmentEntity;

@Data
public class DepartmentModel {
    private static Logger logger = LoggerFactory.getLogger(DepartmentModel.class);
    private String departmentId;
    private String thName;
    private String engName;

    public static DepartmentModel fromEntity(DepartmentEntity entity) {
        if (entity == null) {
            return null;
        }
        DepartmentModel model = new DepartmentModel();
        try {
            model.setDepartmentId(entity.getDepartmentId());
            model.setThName(entity.getThName());
            model.setEngName(entity.getEngName());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static DepartmentEntity toEntity(DepartmentModel model) {
        if (model == null) {
            return null;
        }
        DepartmentEntity entity = new DepartmentEntity();
        try {
            entity.setDepartmentId(model.getDepartmentId());
            entity.setThName(model.getThName());
            entity.setEngName(model.getEngName());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
