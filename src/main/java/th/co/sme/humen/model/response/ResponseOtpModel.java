package th.co.sme.humen.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseOtpModel {
    private String confirmOtp;
    private boolean status;

    public ResponseOtpModel() {

    }
}
