package th.co.sme.humen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import th.co.sme.humen.entity.MemberEntity;
import th.co.sme.humen.exception.ResourceNotFoundException;
import th.co.sme.humen.model.JobModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.repository.JobRepository;
import th.co.sme.humen.repository.MemberRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class JobService {
    @Autowired
    JobRepository jobRepository;

    @Autowired
    MemberRepository memberRepository;

    @Transactional
    public StatusMessage create(JobModel model) {
        if (model.getJobId() != null && !model.getJobId().isEmpty()) {
            this.jobRepository.save(JobModel.toEntity(model));
            return StatusMessage.createInstanceSuccess("Data saved successfully.");
        } else {
            return StatusMessage.createInstanceFail("Data saved unsuccessfully.");
        }
    }

    @Transactional
    public StatusMessage delete(JobModel model) {
        this.jobRepository.findById(model.getJobId())
                .ifPresent(e -> {
                    List<MemberEntity> list = this.memberRepository.findByJob(e)
                            .peek(v -> v.setJob(null))
                            .collect(Collectors.toList());
                    this.memberRepository.saveAll(list);
                    this.jobRepository.delete(e);
                });
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<JobModel> findAllList() {
        return this.jobRepository.findAll(Sort.by(Sort.Direction.ASC, "createdDate"))
                .stream()
                .map(JobModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<JobModel> findPage(PageRequest page) {
        return this.jobRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "jobId")))
                .map(JobModel::fromEntity);
    }

    @Transactional
    public JobModel findById(String jobId) {
        return this.jobRepository.findById(jobId)
                .map(JobModel::fromEntity)
                .orElseThrow(() -> new ResourceNotFoundException("Data " + jobId + " not found"));
    }
}
