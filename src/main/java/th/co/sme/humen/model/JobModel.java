package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.JobEntity;

@Data
public class JobModel {
    private static Logger logger = LoggerFactory.getLogger(JobModel.class);
    private String jobId;
    private String thName;
    private String engName;

    public static JobModel fromEntity(JobEntity entity) {
        if (entity == null) {
            return null;
        }
        JobModel model = new JobModel();
        try {
            model.setJobId(entity.getJobId());
            model.setThName(entity.getThName());
            model.setEngName(entity.getEngName());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static JobEntity toEntity(JobModel model) {
        if (model == null) {
            return null;
        }
        JobEntity entity = new JobEntity();
        try {
            entity.setJobId(model.getJobId());
            entity.setThName(model.getThName());
            entity.setEngName(model.getEngName());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
