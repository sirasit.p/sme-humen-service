package th.co.sme.humen.entity;

import lombok.Data;
import th.co.sme.humen.entity.identity.Eventgrp1Identity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "eventgrp1")
public class Eventgrp1Entity extends AbstractAuditingEntity {
    @EmbeddedId
    private Eventgrp1Identity identity;
    @Column(name = "th_name", columnDefinition = "TEXT")
    private String thName;
    @Column(name = "eng_name", columnDefinition = "TEXT")
    private String engName;
    @Column(name = "limits")
    private Integer limits;
}
