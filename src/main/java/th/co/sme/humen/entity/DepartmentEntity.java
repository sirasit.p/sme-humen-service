package th.co.sme.humen.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "department")
public class DepartmentEntity extends AbstractAuditingEntity {
    @Id
    @NotNull
    @Column(name = "department_id")
    private String departmentId;
    @Column(name = "th_name", columnDefinition = "TEXT")
    private String thName;
    @Column(name = "eng_name", columnDefinition = "TEXT")
    private String engName;
}
