package th.co.sme.humen.model;

import th.co.sme.humen.enums.ActionStatus;

public class StatusMessage {
    private static final String BLANK = "";
    private static StatusMessage DEFAULT_INSTANCE = new StatusMessage(ActionStatus.NONE, BLANK) {
        public void set(ActionStatus state, String message) {
        }

        ;

        public void setActionStatus(ActionStatus state) {
        }

        ;

        public void setMessage(String message) {
        }

        ;
    };
    private static StatusMessage INPUT_INVALID_INSTANCE = new StatusMessage(ActionStatus.NONE, "Input invalid.") {
        public void set(ActionStatus state, String message) {
        }

        ;

        public void setActionStatus(ActionStatus state) {
        }

        ;

        public void setMessage(String message) {
        }

        ;
    };
    private ActionStatus state;
    private String message;
    private int statusCode;
    private Object resultObject;

    public StatusMessage(ActionStatus state, String message) {
        this.setValue(state, message);
    }

    public StatusMessage(ActionStatus state, String message, Object resultObject) {
        this.state = state;
        this.message = message;
        this.resultObject = resultObject;
    }

    public StatusMessage(ActionStatus state, int code, String message) {
        this.setValue(state, code, message);
    }

    private void setValue(ActionStatus state, String message) {
        this.state = state;
        this.message = message;
    }

    private void setValue(ActionStatus state, int code, String message) {
        this.state = state;
        this.message = message;
        this.statusCode = code;
    }

    public void set(ActionStatus state, String message) {
        this.setValue(state, message);
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ActionStatus getActionStatus() {
        return this.state;
    }

    public void setActionStatus(ActionStatus state) {
        this.state = state;
    }

    public Object getResultObject() {
        return this.resultObject;
    }

    public void setResponse(Object response) {
        this.resultObject = response;
    }

    public boolean isSuccess() {
        return this.state.isSuccess();
    }

    public boolean isFail() {
        return this.state.isFail();
    }

    public boolean isNone() {
        return this.state.isNone();
    }

    public static StatusMessage getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static StatusMessage getInputInvalidInstance() {
        return INPUT_INVALID_INSTANCE;
    }

    public static StatusMessage createInstanceSuccess() {
        return createInstanceSuccess(BLANK);
    }

    public static StatusMessage createInstanceSuccess(String message) {
        return new StatusMessage(ActionStatus.SUCCESS, message);
    }

    public static StatusMessage createInstanceSuccess(String message, Object resultObject) {
        return new StatusMessage(ActionStatus.SUCCESS, message, resultObject);
    }

    public static StatusMessage createInstanceFail() {
        return createInstanceFail(BLANK);
    }

    public static StatusMessage createInstanceFail(String message) {
        return new StatusMessage(ActionStatus.FAIL, message);
    }

    public static StatusMessage createInstanceFail(int code, String message) {
        return new StatusMessage(ActionStatus.FAIL, code, message);
    }

    public static StatusMessage createInstanceFail(Exception e) {
        return new StatusMessage(ActionStatus.FAIL, e.getMessage());
    }

    public static StatusMessage createInstanceFail(Exception e, int code) {
        return new StatusMessage(ActionStatus.FAIL, code, e.getMessage());
    }
}
