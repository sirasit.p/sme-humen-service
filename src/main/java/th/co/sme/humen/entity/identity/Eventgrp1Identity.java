package th.co.sme.humen.entity.identity;

import lombok.AllArgsConstructor;
import lombok.Data;
import th.co.sme.humen.entity.EventgrpEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Embeddable
@AllArgsConstructor
public class Eventgrp1Identity  implements Serializable {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eventgrp_id")
    private EventgrpEntity eventgrp;

    @NotNull
    @Column(name = "type")
    private String type;

    public Eventgrp1Identity() {

    }
}
