package th.co.sme.humen.entity.identity;

import lombok.AllArgsConstructor;
import lombok.Data;
import th.co.sme.humen.entity.MemberEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

@Data
@Embeddable
@AllArgsConstructor
public class TimeCurrentIdentity implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private MemberEntity member;

    @NotNull
    @Column(name = "date_id")
    private String dateId;

    public TimeCurrentIdentity() {

    }
}
