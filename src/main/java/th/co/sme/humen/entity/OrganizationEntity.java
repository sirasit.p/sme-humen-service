package th.co.sme.humen.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Entity
@Table(name = "organization")
public class OrganizationEntity extends AbstractAuditingEntity {
    @Id
    @NotNull
    @Column(name = "organization_id")
    private String organizationId;
    @Column(name = "th_name", columnDefinition = "TEXT")
    private String thName;
    @Column(name = "eng_name", columnDefinition = "TEXT")
    private String engName;

    @Column(name = "picture", columnDefinition = "TEXT")
    private String picture;

    @Column(name = "details", columnDefinition = "TEXT")
    private String details;

    @Column(name = "start_time")
    private Double startTime;
    @Column(name = "end_time")
    private Double endTime;

    public OrganizationEntity() {
        ;
    }

    public OrganizationEntity(String organizationId) {
        this.organizationId = organizationId;
    }

}


