package th.co.sme.humen.model.request;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public class ResetPasswordRequestModel {
    private static Logger logger = LoggerFactory.getLogger(ResetPasswordRequestModel.class);
    private String username;
    private String email;
}
