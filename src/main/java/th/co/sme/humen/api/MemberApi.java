package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.anotation.CommonArgument;
import th.co.sme.humen.model.MemberEventgrpModel;
import th.co.sme.humen.model.MemberModel;
import th.co.sme.humen.model.ProfileModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.resolver.CommonParameter;
import th.co.sme.humen.service.MemberService;
import th.co.myhr.security.anotation.UProfileArgument;
import th.co.myhr.system.security.model.UProfile;
import th.co.sme.humen.service.TimeCurrentService;

import javax.mail.MessagingException;
import java.util.List;

@RestController
@EnableAsync
@RequestMapping("/member")
public class MemberApi {
    @Autowired
    MemberService memberService;
    @Autowired
    TimeCurrentService timeCurrentService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody MemberModel request) {
        return this.memberService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody MemberModel request) {
        return this.memberService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<MemberModel> findPage(@CommonArgument CommonParameter param) {
        return this.memberService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<MemberModel> findAllList() {
        return this.memberService.findAllList();
    }

    @GetMapping("/{memberId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public MemberModel findById(@PathVariable(value = "memberId") String memberId) {
        return this.memberService.findById(memberId);
    }

    @GetMapping("/profile")
    @Parameter(name = "up", hidden = true)
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public ProfileModel findByProfile(@UProfileArgument UProfile up) {
        return this.memberService.findByProfile(up);
    }

    @GetMapping("/lists/waiting-check/{status}")
    @Operation(summary = "ตรวจสอบ จำนวน member ตาม status 1 0", security = @SecurityRequirement(name = "bearerAuth"))
    public List<MemberModel> waitingCheck(@PathVariable(value = "status") int status) {
        return this.memberService.waitingCheck(status);
    }

    @GetMapping("/lists/newest")
    @Operation(summary = "จำนวน member ที่มีการสร้างใหม่", security = @SecurityRequirement(name = "bearerAuth"))
    public long countByStatus() {
        return this.memberService.countByStatus();
    }

    @PutMapping("/approve")
    @Operation(summary = "เปิด-ปิด ใช้งาน user member", security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage approveMember(@RequestBody MemberModel member) throws MessagingException {
        return this.memberService.approveMember(member);
    }

    @GetMapping("/leave")
    @Parameter(name = "up", hidden = true)
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<MemberEventgrpModel> getLeaveStatisticByMember(@UProfileArgument UProfile up, @RequestParam(name = "memberId", defaultValue = "") String memberId) {
        return this.timeCurrentService.getMemberLeaveStatistic(memberId.isEmpty() ? up.getEmployeeId() : memberId);
    }
}
