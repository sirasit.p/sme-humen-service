package th.co.sme.humen.model.request;

import lombok.Data;

@Data
public class RefreshTokenRequestModel {
    String refreshToken;
}
