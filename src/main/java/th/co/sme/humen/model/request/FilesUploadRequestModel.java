package th.co.sme.humen.model.request;

import lombok.Data;

@Data
public class FilesUploadRequestModel {
    private String fileName;
    private String fileType;
    private byte[] fileData;
}
