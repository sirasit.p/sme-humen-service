package th.co.sme.humen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.sme.humen.entity.EventgrpEntity;

@Repository
public interface EventgrpRepository extends JpaRepository<EventgrpEntity, String> {
}
