package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.anotation.CommonArgument;
import th.co.sme.humen.model.TagModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.resolver.CommonParameter;
import th.co.sme.humen.service.TagService;

import java.util.List;

@RestController
@RequestMapping("/tag")
public class TagApi {
    @Autowired
    TagService tagService;

    @PostMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody List<TagModel> request) {
        return this.tagService.create(request);
    }

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody TagModel request) {
        return this.tagService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody TagModel request) {
        return this.tagService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<TagModel> findPage(@CommonArgument CommonParameter param) {
        return this.tagService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<TagModel> findAllList() {
        return this.tagService.findAllList();
    }

    @GetMapping("/lists/{textSearch}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<TagModel> findByTextSearch(@PathVariable(value = "textSearch") String textSearch) {
        return this.tagService.findByTextSearch(textSearch);
    }

    @GetMapping("/{value}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public TagModel findById(@PathVariable(value = "value") String value) {
        return this.tagService.findById(value);
    }
}
