package th.co.sme.humen.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponseModel {
    private String username;
    private String accessToken;
    private String refreshToken;
}
