package th.co.sme.humen.entity;

import lombok.Data;
import th.co.sme.humen.entity.identity.TimeCurrentIdentity;

import javax.persistence.*;

@Data
@Entity
@Table(name = "time_current")
public class TimeCurrentEntity extends AbstractAuditingEntity {
    @EmbeddedId
    private TimeCurrentIdentity identity;

    @Column(name = "start_time")
    private Double startTime;

    @Column(name = "end_time")
    private Double endTime;

    @Column(name = "swipe_start_time")
    private Double swipeStartTime;

    @Column(name = "swipe_end_time")
    private Double swipeEndTime;

    @Column(name = "status")
    private Integer status;

    @Column(name = "detail")
    private String detail;

    @Column(name = "eventgrp_type")
    private String eventgrpType;
}
