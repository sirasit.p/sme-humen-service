package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.anotation.CommonArgument;
import th.co.sme.humen.model.*;
import th.co.sme.humen.model.request.TimeCurrentRequestModel;
import th.co.sme.humen.resolver.CommonParameter;
import th.co.sme.humen.service.OrganizationService;
import th.co.sme.humen.service.TimeCurrentService;

import java.util.List;

@RestController
@RequestMapping("/time")
public class TimeCurrentApi {
    @Autowired
    TimeCurrentService timeCurrentService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody List<MemberTimeCurrentModel> request) {
        return this.timeCurrentService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody List<MemberTimeCurrentModel> request) {
        return this.timeCurrentService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<TimeCurrentModel> findPage(@CommonArgument CommonParameter param) {
        return this.timeCurrentService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<TimeCurrentModel> findAllList() {
        return this.timeCurrentService.findAllList();
    }

    @PostMapping("/lists/filter")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<MemberTimeCurrentModel> getTimeCurrent(@RequestBody TimeCurrentRequestModel request) {
        return this.timeCurrentService.getTimeFromMember(request);
    }
}
