package th.co.sme.humen.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "tag")
public class TagEntity extends AbstractAuditingEntity {
    @Id
    @NotNull
    @Column(name = "value", columnDefinition = "TEXT")
    private String value;
}
