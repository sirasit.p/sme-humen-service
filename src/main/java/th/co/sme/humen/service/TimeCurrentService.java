package th.co.sme.humen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import th.co.sme.humen.entity.Eventgrp1Entity;
import th.co.sme.humen.entity.EventgrpEntity;
import th.co.sme.humen.entity.OrganizationEntity;
import th.co.sme.humen.entity.TimeCurrentEntity;
import th.co.sme.humen.enums.TimeStatus;
import th.co.sme.humen.model.*;
import th.co.sme.humen.model.mini.MemberMiniModel;
import th.co.sme.humen.model.request.TimeCurrentRequestModel;
import th.co.sme.humen.repository.Eventgrp1Repository;
import th.co.sme.humen.repository.MemberRepository;
import th.co.sme.humen.repository.OrganizationRepository;
import th.co.sme.humen.repository.TimeCurrentRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TimeCurrentService {
    @Autowired
    TimeCurrentRepository timeCurrentRepository;
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    Eventgrp1Repository eventgrp1Repository;
    @Autowired
    OrganizationRepository organizationRepository;

    @Transactional
    public StatusMessage create(List<MemberTimeCurrentModel> request) {
        List<TimeCurrentEntity> entityList = new ArrayList<>();
        if (!request.isEmpty()) {
            double startTime = 0;
            double endTime = 0;
            Optional<OrganizationEntity> entityOrganization = this.organizationRepository.findById("100");
            if (entityOrganization.isPresent()) {
                OrganizationEntity organization = entityOrganization.get();
                startTime = organization.getStartTime();
                endTime = organization.getEndTime();
            }

            for (MemberTimeCurrentModel member : request) {
                for (TimeCurrentModel timeModel : member.getTime()) {
                    timeModel.setStartTime(startTime);
                    timeModel.setEndTime(endTime);
                    TimeCurrentEntity entityTimeCurrent = TimeCurrentModel.toEntity(member.getMember(), timeModel);
                    entityList.add(entityTimeCurrent);
                }
            }

            if (!entityList.isEmpty()) {
                this.timeCurrentRepository.saveAll(entityList);
            }
        }
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage delete(List<MemberTimeCurrentModel> request) {
        List<TimeCurrentEntity> entityList = new ArrayList<>();
        if (!request.isEmpty()) {
            for (MemberTimeCurrentModel member : request) {
                for (TimeCurrentModel timeModel : member.getTime()) {
                    TimeCurrentEntity entity = TimeCurrentModel.toEntity(member.getMember(), timeModel);
                    entityList.add(entity);
                }
            }

            if (!entityList.isEmpty()) {
                this.timeCurrentRepository.deleteAll(entityList);
            }
        }

        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<MemberTimeCurrentModel> getTimeFromMember(TimeCurrentRequestModel request) {
        int param1, param2, param3, param4, param5, param6, param7, param8;
        param1 = !request.getDepartmentId().isEmpty() ? 1 : 2;
        param2 = param1 == 1 ? 2 : 1;
        param3 = !request.getPositionId().isEmpty() ? 1 : 2;
        param4 = param3 == 1 ? 2 : 1;
        param5 = !request.getJobId().isEmpty() ? 1 : 2;
        param6 = param5 == 1 ? 2 : 1;
        param7 = !request.getMemberId().isEmpty() ? 1 : 2;
        param8 = param7 == 1 ? 2 : 1;

        List<String> memberIds = this.memberRepository.findByMemberFilter(request.getDepartmentId()
                , request.getPositionId()
                , request.getJobId()
                , request.getMemberId()
                , param1, param2, param3, param4, param5, param6, param7, param8);

        return getMemberTimeCurrentModel(memberIds, request.getStartDate(), request.getEndDate());
    }

    @Transactional
    public List<MemberTimeCurrentModel> getMemberTimeCurrentModel(List<String> memberIds, String startDate, String endDate) {
        List<MemberTimeCurrentModel> response = new ArrayList<>();
        if (!memberIds.isEmpty()) {
            for (String memberId : memberIds) {
                this.memberRepository.findById(memberId)
                        .ifPresent(member -> {
                            if (StringUtils.hasText(startDate) && StringUtils.hasText(endDate)) {
                                List<TimeCurrentModel> timeCurrentList = this.timeCurrentRepository.findByIdentityMemberAndIdentityDateIdBetween(member, startDate, endDate, Sort.by(Sort.Direction.ASC, "identity"))
                                        .map(TimeCurrentModel::fromEntity)
                                        .map(this::getStatus)
                                        .collect(Collectors.toList());

                                List<MemberEventgrpModel> eventgrp = this.getMemberLeaveStatistic(member.getMemberId());
                                MemberTimeCurrentModel model = MemberTimeCurrentModel.fromModel(MemberMiniModel.fromEntity(member), timeCurrentList, eventgrp);

                                this.timeCurrentRepository.saveAll(TimeCurrentModel.toEntity(MemberMiniModel.fromEntity(member), timeCurrentList));
                                response.add(model);
                            } else {
                                List<TimeCurrentModel> timeCurrentList = this.timeCurrentRepository.findByIdentityMember(member)
                                        .map(TimeCurrentModel::fromEntity)
                                        .map(this::getStatus)
                                        .collect(Collectors.toList());

                                List<MemberEventgrpModel> eventgrp = this.getMemberLeaveStatistic(member.getMemberId());
                                MemberTimeCurrentModel model = MemberTimeCurrentModel.fromModel(MemberMiniModel.fromEntity(member), timeCurrentList, eventgrp);

                                this.timeCurrentRepository.saveAll(TimeCurrentModel.toEntity(MemberMiniModel.fromEntity(member), timeCurrentList));
                                response.add(model);
                            }

                        });
            }
        }

        return response;
    }

    @Transactional
    public List<MemberEventgrpModel> getMemberLeaveStatistic(String memberId) {
        LocalDate currentDate = LocalDate.now();
        String startDate = LocalDate.of(currentDate.getYear(), 1, 1).toString();
        String endDate = LocalDate.of(currentDate.getYear(), 12, 31).toString();

        List<MemberEventgrpModel> response = new ArrayList<>();
        this.memberRepository.findById(memberId)
                .ifPresent(member -> {
                    if (member.getEventgrp() == null) {
                        member.setEventgrp(new EventgrpEntity("100"));
                        this.memberRepository.save(member);
                        this.memberRepository.flush();
                    }

                    List<EventgrpDetailModel> eventgrpDetail = EventgrpDetailModel.fromEntity(member.getEventgrp().getEventgrp1List());
                    for (EventgrpDetailModel eventgrp : eventgrpDetail) {
                        long useEventgrp = this.timeCurrentRepository.countByIdentityMemberMemberIdAndIdentityDateIdBetweenAndEventgrpType(memberId, startDate, endDate, eventgrp.getType());
                        MemberEventgrpModel memberEventgrp = MemberEventgrpModel.fromEntity(eventgrp, (int) useEventgrp);
                        response.add(memberEventgrp);
                    }
                });

        return response;
    }

    @Transactional
    public TimeCurrentModel getStatus(TimeCurrentModel model) {
        int status = model.getStatus();
        if (status != 0) {
            if (model.getStartTime() < model.getSwipeStartTime()) {
                /*สาย เวลาเข้าตามกะ < เวลาสแกนเข้างานพนักงาน*/
                status = TimeStatus.LATE.getValue();
            }

            if (model.getEndTime() > model.getSwipeEndTime() && model.getSwipeEndTime() > 0) {
                /*ออกก่อน เวลาออกตามกะ > เวลาสแกนออกงานพนักงาน*/
                status = TimeStatus.ABSENT.getValue();
            }

            if (model.getSwipeStartTime() == 0 || model.getSwipeEndTime() == 0) {
                /*ขาด เวลาสแกนเข้าออกงาน */
                status = TimeStatus.ABSENT.getValue();
            }

            if (model.getStartTime() > model.getSwipeStartTime() && model.getEndTime() < model.getSwipeEndTime()
                    && model.getSwipeStartTime() > 0 && model.getSwipeEndTime() > 0) {
                /*ทำงาน เวลาเข้าตามกะ > เวลาสแกนเข้า
                 * เวลาออกตามกะ < เวลาสแกนออก และ เวลาสแกนเข้าออก > 0*/
                status = TimeStatus.WORK.getValue();
            }

            if (!model.getEventgrpType().isEmpty()) {
                /*ลา ตรวจสอข EventgrpType ไม่ว่างให้เป็นลา */
                status = TimeStatus.LEAVE.getValue();
            }
        }

        model.setStatus(status);
        return model;
    }


    @Transactional
    public List<TimeCurrentModel> findAllList() {
        return this.timeCurrentRepository.findAll(Sort.by(Sort.Direction.ASC, "identity"))
                .stream()
                .map(TimeCurrentModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<TimeCurrentModel> findPage(PageRequest page) {
        return this.timeCurrentRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "identity")))
                .map(TimeCurrentModel::fromEntity);
    }

    public List<String> getDatePeriodList(String startDateStr, String endDateStr) {
        /*ใช้สำหรับการหาช่วงวันที่*/
        /*เช่น startDateStr = '2023-10-18' , endDateStr = '2023-10-20'*/
        /*ผลลัพธ์ ['2023-10-18','2023-10-19','2023-10-20']*/
        LocalDate startDate = LocalDate.parse(startDateStr);
        LocalDate endDate = LocalDate.parse(endDateStr);

        long totalDays = ChronoUnit.DAYS.between(startDate, endDate); /*คำนวณจำนวนวันทั้งหมดระหว่างวันที่เริ่มต้นและวันที่สิ้นสุด*/
        List<String> dateList = new ArrayList<>();
        for (int i = 0; i <= totalDays; i++) {
            LocalDate currentDate = startDate.plusDays(i);
            dateList.add(currentDate.toString());
        }

        //System.out.println(startDateStr + " : " + endDateStr + " มีทั้งหมด " + totalDays + " วัน");
        return dateList;
    }
}
