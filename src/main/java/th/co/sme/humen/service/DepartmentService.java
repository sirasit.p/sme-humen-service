package th.co.sme.humen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import th.co.sme.humen.entity.MemberEntity;
import th.co.sme.humen.exception.ResourceNotFoundException;
import th.co.sme.humen.model.DepartmentModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.repository.DepartmentRepository;
import th.co.sme.humen.repository.MemberRepository;

import javax.transaction.Transactional;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    MemberRepository memberRepository;

    @Transactional
    public StatusMessage create(DepartmentModel model) {
        if (model.getDepartmentId() != null && !model.getDepartmentId().isEmpty()) {
            this.departmentRepository.save(DepartmentModel.toEntity(model));
            return StatusMessage.createInstanceSuccess("Data saved successfully.");
        } else {
            return StatusMessage.createInstanceFail("Data saved unsuccessfully.");
        }
    }

    @Transactional
    public StatusMessage delete(DepartmentModel model) {
        this.departmentRepository.findById(model.getDepartmentId())
                .ifPresent(e -> {
                    List<MemberEntity> list = this.memberRepository.findByDepartment(e)
                            .peek(v -> v.setDepartment(null))
                            .collect(Collectors.toList());
                    this.memberRepository.saveAll(list);
                    this.departmentRepository.delete(e);
                });
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<DepartmentModel> findAllList() {
        return this.departmentRepository.findAll(Sort.by(Sort.Direction.ASC, "departmentId"))
                .stream()
                .map(DepartmentModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<DepartmentModel> findPage(PageRequest page) {
        return this.departmentRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "departmentId")))
                .map(DepartmentModel::fromEntity);
    }

    @Transactional
    public DepartmentModel findById(String departmentId) {
        return this.departmentRepository.findById(departmentId)
                .map(DepartmentModel::fromEntity)
                .orElseThrow(() -> new ResourceNotFoundException("Data " + departmentId + " not found"));
    }
}
