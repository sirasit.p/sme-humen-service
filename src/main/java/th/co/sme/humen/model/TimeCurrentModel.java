package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.TimeCurrentEntity;
import th.co.sme.humen.entity.identity.TimeCurrentIdentity;
import th.co.sme.humen.model.mini.MemberMiniModel;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class TimeCurrentModel {
    private static Logger logger = LoggerFactory.getLogger(TimeCurrentModel.class);
    private String dateId;
    private double startTime;
    private double endTime;
    private double swipeStartTime;
    private double swipeEndTime;
    private Integer status;
    private String detail;
    private String eventgrpType;

    public static List<TimeCurrentEntity> toEntity(MemberMiniModel member, List<TimeCurrentModel> model) {
        if (model == null) {
            return null;
        }
        return model.stream().map(time -> TimeCurrentModel.toEntity(member, time)).collect(Collectors.toList());
    }

    public static TimeCurrentEntity toEntity(MemberMiniModel member, TimeCurrentModel model) {
        if (model == null) {
            return null;
        }

        TimeCurrentEntity entity = new TimeCurrentEntity();
        try {
            entity.setIdentity(new TimeCurrentIdentity(MemberMiniModel.toEntity(member), model.getDateId()));
            entity.setStartTime(model.getStartTime());
            entity.setEndTime(model.getEndTime());
            entity.setSwipeStartTime(model.getSwipeStartTime());
            entity.setSwipeEndTime(model.getSwipeEndTime());
            entity.setStatus(model.getStatus());
            entity.setDetail(model.getDetail());
            entity.setEventgrpType(model.getEventgrpType());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return entity;
    }

    public static TimeCurrentModel fromEntity(TimeCurrentEntity entity) {
        if (entity == null) {
            return null;
        }

        TimeCurrentModel model = new TimeCurrentModel();
        try {
            model.setDateId(entity.getIdentity().getDateId());
            model.setStartTime(entity.getStartTime());
            model.setEndTime(entity.getEndTime());
            if (Optional.ofNullable(entity.getSwipeStartTime()).isPresent()) {
                model.setSwipeStartTime(entity.getSwipeStartTime());
            } else {
                model.setSwipeStartTime(0.0);
            }
            if (Optional.ofNullable(entity.getSwipeStartTime()).isPresent()) {
                model.setSwipeEndTime(entity.getSwipeEndTime());
            } else {
                model.setSwipeEndTime(0.0);
            }

            model.setStatus(entity.getStatus());
            model.setDetail(entity.getDetail());
            model.setEventgrpType(entity.getEventgrpType());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return model;
    }
}
