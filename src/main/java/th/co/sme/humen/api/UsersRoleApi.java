package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.anotation.CommonArgument;
import th.co.sme.humen.model.UsersRoleModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.resolver.CommonParameter;
import th.co.sme.humen.service.UsersRoleService;

import java.util.List;

@RestController
@RequestMapping("/users-role")
public class UsersRoleApi {
    @Autowired
    UsersRoleService roleService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody UsersRoleModel request) {
        return this.roleService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody UsersRoleModel request) {
        return this.roleService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<UsersRoleModel> findPage(@CommonArgument CommonParameter param) {
        return this.roleService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<UsersRoleModel> findAllList() {
        return this.roleService.findAllList();
    }

    @GetMapping("/{roleId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public UsersRoleModel findById(@PathVariable(value = "roleId") String roleId) {
        return this.roleService.findById(roleId);
    }
}
