package th.co.sme.humen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import th.co.sme.humen.entity.*;

import java.util.List;
import java.util.stream.Stream;

@Repository
public interface MemberRepository extends JpaRepository<MemberEntity, String> {
//    Stream<MemberEntity> findByOrganization(OrganizationEntity organization);

    Stream<MemberEntity> findByDepartment(DepartmentEntity department);

    Stream<MemberEntity> findByPosition(PositionEntity position);

    Stream<MemberEntity> findByJob(JobEntity job);

    Stream<MemberEntity> findByStatus(int status, Sort sort);

    long countByStatus(int status);

//    @Query(value = "select distinct(m.member_id) from member m where member_id like '%' " +
//            "  and ((1 = :param1 and organization_id = :organizationId) or (1 = :param2)) " +
//            "  and ((1 = :param3 and department_id = :departmentId) or (1 = :param4)) " +
//            "  and ((1 = :param5 and position_id = :positionId) or (1 = :param6)) " +
//            "  and ((1 = :param7 and job_id = :jobId) or (1 = :param8)) " +
//            "  and status = 1 ", nativeQuery = true)
//    List<String> findByMemberFilter(@Param("organizationId") String organizationId
//            , @Param("departmentId") String departmentId
//            , @Param("positionId") String positionId
//            , @Param("jobId") String jobId
//            , @Param("param1") int param1
//            , @Param("param2") int param2
//            , @Param("param3") int param3
//            , @Param("param4") int param4
//            , @Param("param5") int param5
//            , @Param("param6") int param6
//            , @Param("param7") int param7
//            , @Param("param8") int param8);

    @Query(value = "select distinct(m.member_id) from member m where member_id like '%' " +
            "  and ((1 = :param1 and department_id = :departmentId) or (1 = :param2)) " +
            "  and ((1 = :param3 and position_id = :positionId) or (1 = :param4)) " +
            "  and ((1 = :param5 and job_id = :jobId) or (1 = :param6)) " +
            "  and ((1 = :param7 and member_id = :memberId) or (1 = :param8)) " +
            "  and status = 1 ", nativeQuery = true)
    List<String> findByMemberFilter(@Param("departmentId") String departmentId
            , @Param("positionId") String positionId
            , @Param("jobId") String jobId
            , @Param("memberId") String memberId
            , @Param("param1") int param1
            , @Param("param2") int param2
            , @Param("param3") int param3
            , @Param("param4") int param4
            , @Param("param5") int param5
            , @Param("param6") int param6
            , @Param("param7") int param7
            , @Param("param8") int param8);
}
