package th.co.sme.humen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmeHumenServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmeHumenServiceApplication.class, args);
    }

}
