package th.co.sme.humen.model.request;

import lombok.Data;

@Data
public class QrCodeGenerateRequestModel {
    private int width;
    private int height;
    private String text;
}
