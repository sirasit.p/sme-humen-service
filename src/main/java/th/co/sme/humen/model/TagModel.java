package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.TagEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class TagModel {
    private static Logger logger = LoggerFactory.getLogger(TagModel.class);
    private String display;
    private String value;

    public static TagModel fromEntity(TagEntity entity) {
        if (entity == null) {
            return null;
        }
        TagModel model = new TagModel();
        try {
            model.setDisplay(entity.getValue());
            model.setValue(entity.getValue());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static TagEntity toEntity(TagModel model) {
        if (model == null) {
            return null;
        }
        TagEntity entity = new TagEntity();
        try {
            entity.setValue(model.getValue());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }

    public static List<TagModel> fromEntity(List<String> list) {
        if (list == null) {
            return new ArrayList<>();
        }
        return list.stream()
                .map(TagModel::fromEntity)
                .collect(Collectors.toList());
    }

    public static TagModel fromEntity(String value) {
        TagModel model = new TagModel();
        try {
            model.setDisplay(value);
            model.setValue(value);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }
}
