package th.co.sme.humen.service;

import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.model.request.FilesUploadRequestModel;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.UUID;

@Service
public class FilesService {
    @Value("${image.extensions}")
    private String imageExtensions;
    @Value("${vdo.extensions}")
    private String vdoExtensions;
    @Value("${pdf.extensions}")
    private String pdfExtensions;

    @Autowired
    FilesStorageService filesStorageService;

    @Transactional
    public StatusMessage uploadImage(MultipartFile file) {
        this.filesStorageService.init("image");
//        if (Files.getFileExtension(file.getOriginalFilename()) == null) {
//            return StatusMessage.createInstanceFail("Invalid upload file");
//        } else {
//            String fileExtensions = Files.getFileExtension(file.getOriginalFilename());
//            if (!imageExtensions.contains(fileExtensions)) {
//                return StatusMessage.createInstanceFail("Please upload file : " + imageExtensions);
//            }
//        }
        UUID uuid = UUID.randomUUID();
        String filename = uuid + "." + Files.getFileExtension(file.getOriginalFilename());
        this.filesStorageService.save(file, filename);
        return StatusMessage.createInstanceSuccess("Data upload successfully.", filename);
    }

    @Transactional
    public byte[] openImage(String filename) throws IOException {
        this.filesStorageService.init("image");
        Resource file = this.filesStorageService.load(filename);
        if (file.getFile().isFile()) {
            InputStream in = file.getInputStream();
            return IOUtils.toByteArray(in);
        } else {
            return new byte[]{};
        }
    }

    @Transactional
    public StatusMessage uploadVideo(MultipartFile file) {
        this.filesStorageService.init("video");
//        if (Files.getFileExtension(file.getOriginalFilename()) == null) {
//            return StatusMessage.createInstanceFail("Invalid upload file");
//        } else {
//            String fileExtensions = Files.getFileExtension(file.getOriginalFilename());
//            if (!vdoExtensions.contains(fileExtensions)) {
//                return StatusMessage.createInstanceFail("Please upload file : " + vdoExtensions);
//            }
//        }
        UUID uuid = UUID.randomUUID();
        String filename = uuid + "." + Files.getFileExtension(file.getOriginalFilename());
        this.filesStorageService.save(file, filename);
        return StatusMessage.createInstanceSuccess("Data upload successfully.", filename);
    }

    @Transactional
    public FileSystemResource openVideo(String filename) throws IOException {
        this.filesStorageService.init("video");
        Resource file = this.filesStorageService.load(filename);
        return new FileSystemResource(file.getFile());
    }

    @Transactional
    public StatusMessage uploadPdf(MultipartFile file) {
        this.filesStorageService.init("pdf");
//        if (Files.getFileExtension(file.getOriginalFilename()) == null) {
//            return StatusMessage.createInstanceFail("Invalid upload file");
//        } else {
//            String fileExtensions = Files.getFileExtension(file.getOriginalFilename());
//            if (!pdfExtensions.contains(fileExtensions)) {
//                return StatusMessage.createInstanceFail("Please upload file : " + pdfExtensions);
//            }
//        }
        UUID uuid = UUID.randomUUID();
        String filename = uuid + "." + Files.getFileExtension(file.getOriginalFilename());
        this.filesStorageService.save(file, filename);
        return StatusMessage.createInstanceSuccess("Data upload successfully.", filename);
    }

    @Transactional
    public FileSystemResource openPdf(String filename) throws IOException {
        this.filesStorageService.init("pdf");
        Resource file = this.filesStorageService.load(filename);
        return new FileSystemResource(file.getFile());
    }

    @Transactional
    public StatusMessage convertFileToBase64(MultipartFile file) throws IOException {
        return StatusMessage.createInstanceSuccess(Base64.getEncoder().encodeToString(file.getBytes()));
    }

    @Transactional
    public StatusMessage uploadBase64(FilesUploadRequestModel request) {
        this.filesStorageService.init(request.getFileType());
        this.filesStorageService.save(request.getFileData(), request.getFileName());
        return StatusMessage.createInstanceSuccess("Data upload successfully.", request.getFileName());
    }

    @Transactional
    public StatusMessage uploadCertificate(MultipartFile file, String filename) {
        this.filesStorageService.init("certificate");
//        if (Files.getFileExtension(file.getOriginalFilename()) == null) {
//            return StatusMessage.createInstanceFail("Invalid upload file");
//        } else {
//            String fileExtensions = Files.getFileExtension(file.getOriginalFilename());
//            if (!pdfExtensions.contains(fileExtensions)) {
//                return StatusMessage.createInstanceFail("Please upload file : " + pdfExtensions);
//            }
//        }
        this.filesStorageService.save(file, filename);
        return StatusMessage.createInstanceSuccess("Data upload successfully.", filename);
    }

    @Transactional
    public StatusMessage uploadCertificateBase64(FilesUploadRequestModel request) {
        this.filesStorageService.init("certificate");
        this.filesStorageService.save(request.getFileData(), request.getFileName());
        return StatusMessage.createInstanceSuccess("Data upload successfully.", request.getFileName());
    }

    @Transactional
    public FileSystemResource openCertificate(String filename) throws IOException {
        this.filesStorageService.init("certificate");
        Resource file = this.filesStorageService.load(filename);
        return new FileSystemResource(file.getFile());
    }

    @Transactional
    public StatusMessage upload(MultipartFile file) {
        this.filesStorageService.init("file");
        UUID uuid = UUID.randomUUID();
        String filename = uuid + "." + Files.getFileExtension(file.getOriginalFilename());
        this.filesStorageService.save(file, filename);
        return StatusMessage.createInstanceSuccess("Data upload successfully.", filename);
    }

    @Transactional
    public FileSystemResource download(String filename) throws IOException {
        this.filesStorageService.init("file");
        Resource file = this.filesStorageService.load(filename);
        return new FileSystemResource(file.getFile());
    }
}
