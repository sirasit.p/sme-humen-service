package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import th.co.sme.humen.anotation.CommonArgument;
import th.co.sme.humen.model.PositionModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.resolver.CommonParameter;
import th.co.sme.humen.service.PositionService;

import java.util.List;

@RestController
@RequestMapping("/position")
public class PositionApi {
    @Autowired
    PositionService positionService;

    @PostMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage create(@RequestBody PositionModel request) {
        return this.positionService.create(request);
    }

    @DeleteMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage delete(@RequestBody PositionModel request) {
        return this.positionService.delete(request);
    }

    @GetMapping
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public Page<PositionModel> findPage(@CommonArgument CommonParameter param) {
        return this.positionService.findPage(PageRequest.of(param.getPage(), param.getSize()));
    }

    @GetMapping("/lists")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public List<PositionModel> findAllList() {
        return this.positionService.findAllList();
    }

    @GetMapping("/{positionId}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public PositionModel findById(@PathVariable(value = "positionId") String positionId) {
        return this.positionService.findById(positionId);
    }
}
