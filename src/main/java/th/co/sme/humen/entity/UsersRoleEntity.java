package th.co.sme.humen.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "users_role")
public class UsersRoleEntity extends AbstractAuditingEntity {
    @Id
    @NotNull
    @Column(name = "role_id")
    private String roleId;
    @Column(name = "th_name", columnDefinition = "TEXT")
    private String thName;
    @Column(name = "eng_name", columnDefinition = "TEXT")
    private String engName;
    @Column(name = "menu", columnDefinition = "TEXT")
    private String menu;
}
