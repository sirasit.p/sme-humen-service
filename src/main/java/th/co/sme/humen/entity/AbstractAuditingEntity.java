package th.co.sme.humen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.context.SecurityContextHolder;
import th.co.myhr.system.security.model.UProfile;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditingEntity implements Serializable {
    private static Logger logger = LoggerFactory.getLogger(AbstractAuditingEntity.class);

    @CreatedBy
    @Column(name = "created_by", length = 50, updatable = false)
    @JsonIgnore
    private String createdBy;

    @CreatedDate
    @Column(name = "created_date", updatable = false)
    @JsonIgnore
    private Instant createdDate = Instant.now();

    @LastModifiedBy
    @Column(name = "last_modified_by", length = 50)
    @JsonIgnore
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    @JsonIgnore
    private Instant lastModifiedDate = Instant.now();

    @PrePersist
    public void onPrePersist() {
        setCreatedDate(Instant.now());
        setLastModifiedDate(Instant.now());
        try {
            UProfile uProfile = (UProfile) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            setCreatedBy(uProfile.getUsername());
            setLastModifiedBy(uProfile.getUsername());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @PreUpdate
    public void onPreUpdate() {
        setCreatedDate(Instant.now());
        setLastModifiedDate(Instant.now());
        try {
            UProfile uProfile = (UProfile) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            setLastModifiedBy(uProfile.getUsername());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
