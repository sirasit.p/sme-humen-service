package th.co.sme.humen.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import th.co.sme.humen.exception.ResourceNotFoundException;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.model.TagModel;
import th.co.sme.humen.repository.TagRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TagService {
    private static Logger logger = LoggerFactory.getLogger(TagService.class);
    @Autowired
    TagRepository tagRepository;

    @Transactional
    public StatusMessage create(List<TagModel> tags) {
        if (Optional.ofNullable(tags).isPresent()) {
            for (TagModel e : tags) {
                this.create(e);
            }
        }
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage create(TagModel tag) {
        if (Optional.ofNullable(tag).isPresent()) {
            this.tagRepository.save(TagModel.toEntity(tag));
        }
        return StatusMessage.createInstanceSuccess("Data saved successfully.");
    }

    @Transactional
    public StatusMessage delete(TagModel model) {
        this.tagRepository.findById(model.getValue())
                .ifPresent(e -> this.tagRepository.delete(e));
        return StatusMessage.createInstanceSuccess("Data deleted successfully.");
    }

    @Transactional
    public List<TagModel> findAllList() {
        return this.tagRepository.findAll(Sort.by(Sort.Direction.ASC, "value"))
                .stream()
                .map(TagModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<TagModel> findByTextSearch(String textSearch) {
        return this.tagRepository.findByValueContaining(textSearch, Sort.by(Sort.Direction.ASC, "value"))
                .map(TagModel::fromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Page<TagModel> findPage(PageRequest page) {
        return this.tagRepository.findAll(page.withSort(Sort.by(Sort.Direction.ASC, "value")))
                .map(TagModel::fromEntity);
    }

    @Transactional
    public TagModel findById(String value) {
        return this.tagRepository.findById(value)
                .map(TagModel::fromEntity)
                .orElseThrow(() -> new ResourceNotFoundException("Data " + value + " not found"));
    }
}
