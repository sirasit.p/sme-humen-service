package th.co.sme.humen.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.sme.humen.model.request.RefreshTokenRequestModel;
import th.co.sme.humen.model.request.RegisterRequestModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.model.request.LoginRequestModel;
import th.co.sme.humen.model.request.ResetPasswordRequestModel;
import th.co.sme.humen.model.response.LoginResponseModel;
import th.co.sme.humen.model.response.ResponseOtpModel;
import th.co.sme.humen.service.AuthService;
import th.co.sme.humen.service.UsersService;

import javax.mail.MessagingException;
import java.io.IOException;

@RestController
@EnableAsync
@RequestMapping("/auth")
public class AuthApi {
    @Autowired
    AuthService authService;
    @Autowired
    UsersService usersService;

    @PostMapping("/login")
    public LoginResponseModel login(@RequestBody LoginRequestModel request) {
        return this.authService.login(request.getUsername(), request.getPassword());
    }

    @PostMapping("/refresh-token")
    public LoginResponseModel refreshToken(@RequestBody RefreshTokenRequestModel request) {
        return this.authService.refreshToken(request.getRefreshToken());
    }

    @PostMapping("/validate-register")
    public StatusMessage validateRegister(@RequestBody RegisterRequestModel request) {
        return this.authService.validateRegister(request);
    }

    @PostMapping("/register")
    public StatusMessage register(@RequestBody RegisterRequestModel request) {
        return this.authService.register(request);
    }

    @PostMapping("/reset-password")
    public StatusMessage resetPassword(@RequestBody ResetPasswordRequestModel request) throws MessagingException, IOException {
        return this.authService.resetPassword(request.getUsername(), request.getEmail());
    }

    @PostMapping("/confirm-otp")
    public ResponseOtpModel confirm(@RequestBody RegisterRequestModel request) throws MessagingException {
        return this.authService.confirm(request);
    }
}
