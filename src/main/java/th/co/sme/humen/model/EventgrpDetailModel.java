package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.Eventgrp1Entity;
import th.co.sme.humen.entity.identity.Eventgrp1Identity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class EventgrpDetailModel {
    private static Logger logger = LoggerFactory.getLogger(EventgrpDetailModel.class);
    private String type;
    private String thName;
    private String engName;
    private int limits;

    public static List<EventgrpDetailModel> fromEntity(List<Eventgrp1Entity> entitys) {
        if (entitys == null) {
            return new ArrayList<>();
        }
        return entitys.stream().map(EventgrpDetailModel::fromEntity).collect(Collectors.toList());
    }

    public static EventgrpDetailModel fromEntity(Eventgrp1Entity entity) {
        if (entity == null) {
            return null;
        }
        EventgrpDetailModel model = new EventgrpDetailModel();
        try {
            model.setType(entity.getIdentity().getType());
            model.setThName(entity.getThName());
            model.setEngName(entity.getEngName());
            model.setLimits(entity.getLimits());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static List<Eventgrp1Entity> toEntity(List<EventgrpDetailModel> models) {
        if (models == null) {
            return new ArrayList<>();
        }
        return models.stream().map(EventgrpDetailModel::toEntity).collect(Collectors.toList());
    }

    public static Eventgrp1Entity toEntity(EventgrpDetailModel model) {
        if (model == null) {
            return null;
        }
        Eventgrp1Entity entity = new Eventgrp1Entity();
        try {
            entity.setIdentity(new Eventgrp1Identity(null, model.getType()));
            entity.setThName(model.getThName());
            entity.setEngName(model.getEngName());
            entity.setLimits(model.getLimits());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
