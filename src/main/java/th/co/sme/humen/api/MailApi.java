package th.co.sme.humen.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.model.request.SendMailRequestModel;
import th.co.sme.humen.service.SendMailService;

import javax.mail.MessagingException;

@RestController
@EnableAsync
@RequestMapping("/mail")
public class MailApi {
    @Autowired
    SendMailService sendMailService;

    @PostMapping("/send")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public StatusMessage sendEmail(@RequestBody SendMailRequestModel request) throws MessagingException {
        this.sendMailService.sendEmail(request.getTo(), request.getCc(), request.getSubject(), request.getContent());
        return StatusMessage.createInstanceSuccess("Send mail successfully.");
    }
}
