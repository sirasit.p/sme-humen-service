package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.UsersRoleEntity;

@Data
public class UsersRoleModel {
    private static Logger logger = LoggerFactory.getLogger(UsersRoleModel.class);
    private String roleId;
    private String thName;
    private String engName;
    private String menu;

    public static UsersRoleModel fromEntity(UsersRoleEntity entity) {
        if (entity == null) {
            return null;
        }
        UsersRoleModel model = new UsersRoleModel();
        try {
            model.setRoleId(entity.getRoleId());
            model.setThName(entity.getThName());
            model.setEngName(entity.getEngName());
            model.setMenu(entity.getMenu());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static UsersRoleEntity toEntity(UsersRoleModel model) {
        if (model == null) {
            return null;
        }
        UsersRoleEntity entity = new UsersRoleEntity();
        try {
            entity.setRoleId(model.getRoleId());
            entity.setThName(model.getThName());
            entity.setEngName(model.getEngName());
            entity.setMenu(model.getMenu());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
