package th.co.sme.humen.resolver;

import lombok.AllArgsConstructor;
import lombok.Data;
import th.co.myhr.system.security.model.UProfile;

@Data
@AllArgsConstructor
public class CommonParameter {
    private int page;
    private int size;
    private String memberId;

    public String getMemberId(UProfile up) {
        return memberId.equals("") ? up.getEmployeeId() : this.memberId;
    }
}
