package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.MemberEntity;

@Data
public class ProfileModel extends MemberModel {
    private static Logger logger = LoggerFactory.getLogger(ProfileModel.class);
    private UsersRoleModel role;

    public static ProfileModel fromEntity(MemberEntity entity) {
        if (entity == null) {
            return null;
        }
        ProfileModel model = new ProfileModel();
        try {
            model.setMemberId(entity.getMemberId());
            model.setPrefix(entity.getPrefix());
            model.setThFirstname(entity.getThFirstname());
            model.setEngFirstname(entity.getEngFirstname());
            model.setThLastname(entity.getThLastname());
            model.setEngLastname(entity.getEngLastname());
            model.setGender(entity.getGender());
            model.setEmail(entity.getEmail());
            model.setTel(entity.getTel());
            model.setPicture(entity.getPicture());
            model.setCode(entity.getCode());
            model.setArea(entity.getArea());
            model.setStatus(entity.getStatus());
            model.setDepartment(DepartmentModel.fromEntity(entity.getDepartment()));
            model.setPosition(PositionModel.fromEntity(entity.getPosition()));
            model.setJob(JobModel.fromEntity(entity.getJob()));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }
}
