package th.co.sme.humen.enums;

public enum TimeStatus {
    WAITING(0), /*รออนุมัติ approval*/
    LEAVE(1), /*ลา*/
    WORK(2), /*ทำงาน*/
    ABSENT(3), /*ขาด*/
    LATE(4), /*สาย*/
    OUT_FIRST(5), /*ออกก่อน*/
    APPROVAL(6); /*อนุมัติ*/

    private final int id;

    TimeStatus(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }

    public static TimeStatus as(String str) {
        for (TimeStatus me : TimeStatus.values()) {
            if (me.name().equalsIgnoreCase(str))
                return me;
        }
        return WAITING;
    }
}
