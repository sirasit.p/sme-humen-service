package th.co.sme.humen.model;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.sme.humen.entity.EventgrpEntity;

import java.util.List;

@Data
public class EventgrpModel {
    private static Logger logger = LoggerFactory.getLogger(EventgrpModel.class);
    private String eventgrpId;
    private String thName;
    private String engName;
    private List<EventgrpDetailModel> detail;

    public static EventgrpModel fromEntity(EventgrpEntity entity) {
        if (entity == null) {
            return null;
        }
        EventgrpModel model = new EventgrpModel();
        try {
            model.setEventgrpId(entity.getEventgrpId());
            model.setThName(entity.getThName());
            model.setEngName(entity.getEngName());
            model.setDetail(EventgrpDetailModel.fromEntity(entity.getEventgrp1List()));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return model;
    }

    public static EventgrpEntity toEntity(EventgrpModel model) {
        if (model == null) {
            return null;
        }
        EventgrpEntity entity = new EventgrpEntity();
        try {
            entity.setEventgrpId(model.getEventgrpId());
            entity.setThName(model.getThName());
            entity.setEngName(model.getEngName());
            //entity.setEventgrp1List(Eventgrp1Model.toEntity(model.getDetail()));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return entity;
    }
}
