package th.co.sme.humen.service;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import th.co.sme.humen.entity.*;
import th.co.sme.humen.enums.EnableStatus;
import th.co.sme.humen.model.request.RegisterRequestModel;
import th.co.sme.humen.model.response.LoginResponseModel;
import th.co.sme.humen.model.response.ResponseOtpModel;
import th.co.sme.humen.repository.MemberRepository;
import th.co.sme.humen.repository.UsersRepository;
import th.co.myhr.system.security.model.UProfile;
import th.co.myhr.system.security.service.TokenService;
import th.co.sme.humen.model.MemberModel;
import th.co.sme.humen.model.StatusMessage;
import th.co.sme.humen.model.UsersModel;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.*;

@Service
public class AuthService {
    @Autowired
    AuthenticationManager authManager;
    @Qualifier("getAccessTokenService")
    @Autowired
    TokenService accessTokenService;
    @Qualifier("getRefreshTokenService")
    @Autowired
    TokenService refreshTokenService;
    @Autowired
    UsersService usersService;
    @Autowired
    SendMailService sendMailService;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    MemberRepository memberRepository;

    @Transactional
    public LoginResponseModel login(String username, String password) {
        try {
            UsersEntity usersEntity = null;
            Instant now = Instant.now();
            boolean isMatches = false;
            Optional<UsersEntity> optionalUsers = this.usersRepository.findByUsernameAndTempPasswordExpireAfter(username, now);
            if (optionalUsers.isPresent()) {
                UsersEntity tempUser = optionalUsers.get();
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                isMatches = passwordEncoder.matches(password, tempUser.getTempPassword());
                if (isMatches) {
                    usersEntity = tempUser;
                }
            }

            if (!isMatches) {
                Authentication authentication = authManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                username, password
                        )
                );
                usersEntity = (UsersEntity) authentication.getPrincipal();
            }
            if (usersEntity.getMember().getStatus() == EnableStatus.ENABLE.getValue()) {
                return this.genToken(UsersModel.fromEntity(usersEntity));
            } else {
                throw new AuthenticationServiceException("Authentication UProfile not match");
            }
        } catch (BadCredentialsException ex) {
            throw new AuthenticationServiceException("Authentication UProfile not match");
        }
    }

    @Transactional
    public LoginResponseModel refreshToken(String oldRefreshToken) {
        try {
            UProfile refreshUp = this.refreshTokenService.parseToken(oldRefreshToken);
            UsersModel usersModel = this.usersService.findById(refreshUp.getUsername());
            return this.genToken(usersModel);
        } catch (BadCredentialsException ex) {
            throw new AuthenticationServiceException("Authentication UProfile not match");
        }
    }

    @Transactional
    public LoginResponseModel genToken(UsersModel usersModel) {
        Map<String, String> claim = new HashMap<>();
        List<String> roles = new ArrayList<>();
        if (Optional.ofNullable(usersModel.getRole()).isPresent()) {
            roles.add(usersModel.getRole().getRoleId());
        }
        UProfile up = new UProfile(usersModel.getUsername(), usersModel.getUsername(), usersModel.getUsername(), usersModel.getMember().getMemberId(), usersModel.getMember().getMemberId(), roles, claim);
        String accessToken = this.accessTokenService.generateToken(up);
        String refreshToken = this.refreshTokenService.generateToken(up);
        return new LoginResponseModel(usersModel.getUsername(), accessToken, refreshToken);
    }

    @Transactional
    public StatusMessage validateRegister(RegisterRequestModel model) {
        UsersModel usersModel = this.usersService.findById(model.getUsername());
        if (model.getUsername().equals(usersModel.getUsername())) {
            return StatusMessage.createInstanceFail("Username is duplicate");
        } else {
            return StatusMessage.createInstanceSuccess("Register successfully.");
        }
    }

    @Transactional
    public StatusMessage register(RegisterRequestModel model) {
        Optional<UsersEntity> optionalUsers = this.usersRepository.findById(model.getUsername());
        if (optionalUsers.isPresent()) {
            return StatusMessage.createInstanceFail("Username is duplicate");
        } else {
            UsersModel user = RegisterRequestModel.toUser(model);
            MemberEntity memberEntity = MemberModel.toEntity(user.getMember());
            memberEntity.setStatus(EnableStatus.DISABLE.getValue());
            memberEntity.setEventgrp(new EventgrpEntity("100")); /*ประเภทการลา*/
            memberEntity.setOrganization(new OrganizationEntity("100")); /*กะการทำงาน เวลาเข้า เวลาออก*/

            memberEntity = this.memberRepository.save(memberEntity);
            user.setMember(MemberModel.fromEntity(memberEntity));
            this.usersService.create(user);
            return StatusMessage.createInstanceSuccess("Register successfully.");
        }
    }

    @Transactional
    public StatusMessage resetPassword(String username, String email) throws MessagingException {
        UsersModel usersModel = this.usersService.findById(username);
        if (email.equals(usersModel.getMember().getEmail())) {
            String tempPassword = RandomStringUtils.randomAlphanumeric(8);
            Calendar tempPasswordExpire = Calendar.getInstance();
            tempPasswordExpire.add(Calendar.HOUR_OF_DAY, 1);
            this.usersService.createTempPassword(usersModel.getUsername(), tempPassword, tempPasswordExpire.toInstant());
            String subject = "Reset password your sme-humen";
            String content = "กรุณา Login your sme-humen ด้วย password : " + tempPassword + " แล้วนำรหัสผ่านนี้ไปใช้ในการเปลี่ยนรหัสผ่านใหม่ \n รหัสผ่านที่นี้มีอายุ 1 ชั่วโมง";
            this.sendMailService.sendEmail(new String[]{usersModel.getMember().getEmail()}, new String[]{}, subject, content);
        } else {
            return StatusMessage.createInstanceFail("Email is incorrect");
        }
        return StatusMessage.createInstanceSuccess("Reset password successfully.");
    }


    @Transactional
    public ResponseOtpModel confirm(RegisterRequestModel model) throws MessagingException {
        ResponseOtpModel otpModel = new ResponseOtpModel();
        otpModel.setStatus(false);
        if (!model.getMember().getEmail().isEmpty()) {
            String otpNumber = genOTP(6);
            otpModel.setConfirmOtp(otpNumber);

            String subject = "Confirm OTP sme-humen";
            String content = "OTP : " + otpNumber;
            this.sendMailService.sendEmail(new String[]{model.getMember().getEmail()}, new String[]{}, subject, content);
            otpModel.setStatus(true);
        }
        return otpModel;
    }

    public String genOTP(int len) {
        String numbers = "0123456789";
        Random random_method = new Random();
        StringBuilder otp = new StringBuilder();
        for (int i = 0; i < len; i++) {
            otp.append(numbers.charAt(random_method.nextInt(numbers.length())));
        }
        return otp.toString();
    }
}
